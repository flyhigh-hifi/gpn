import * as path from 'path';
import * as TypeORM from "typeorm";
import { Container } from 'typedi';

TypeORM.useContainer(Container);

export const createTestConnection = () => {
    return TypeORM.createConnection({
        type: "mysql",
        database: process.env.MYSQL_DATABASE || 'books-test',
        username: process.env.MYSQL_USER || 'root',
        password: process.env.MYSQL_PASSWORD || 'root',
        port: 3306,
        host: process.env.MYSQL_HOST || 'localhost',
        entities: [path.resolve("./src/entities/*.*")],
        synchronize: true,
        dropSchema: true
    });
};
