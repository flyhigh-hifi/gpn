#### Configuration & lauch:
- Specify any options in docker-compose, by default it exposes 3000 and 3306 to host
- Start with `docker-compose up`


#### Test
- This repo contains resolver tests which can be run with `yarn test`, although the used library **type-graphql-dataloader** has bug(probably context wrong live time), so some tests don't work until fix

