import { Book } from '../entities/book';
import {
  Arg,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  Root,
} from 'type-graphql';
import { Author } from '../entities/author';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { BookInput } from '../types/book-input';

@Resolver((of) => Book)
export class BookResolver {
  constructor(
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(Author)
    private readonly authorRepository: Repository<Author>,
  ) {}

  @Mutation((returns) => Book)
  async createBook(@Arg('book') bookInput: BookInput): Promise<Book> {
    const book = this.bookRepository.create({ ...bookInput });
    return this.bookRepository.save(book);
  }

  @Query((returns) => [Book])
  async getBooks(
    @Arg('authorId', { nullable: true }) authorId?: number,
  ): Promise<Book[]> {
    const findArguments = authorId ? { authorId } : {};
    return this.bookRepository.find(findArguments);
  }
}
