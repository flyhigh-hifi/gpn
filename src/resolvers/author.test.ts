import { Connection } from 'typeorm';
import * as faker from 'faker';
import { createTestConnection } from '../../test/utils/createTestConnection';
import { gCall } from '../../test/utils/gCall';
import { Author } from '../entities/author';

let conn: Connection;

beforeAll(async () => {
  conn = await createTestConnection();
});
afterAll(async () => {
  await conn.close();
});

const createAuthorMutation = `
    mutation Create($author: AuthorInput!) {
      createAuthor(author: $author) {
        id
        name
      }
    }
`;

describe('Author', () => {
  it('creates author', async () => {
    const author = {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    };

    const response = await gCall({
      source: createAuthorMutation,
      variableValues: {
        author,
      },
    });

    expect(response?.data?.createAuthor).toMatchObject(author);

    const dbAuthor = await conn
      .getRepository(Author)
      .findOne({ where: { name: author.name } });
    expect(dbAuthor).toBeDefined();
    expect(dbAuthor!.name).toBe(author.name);
  });
});
