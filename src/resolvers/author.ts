import { Book } from '../entities/book';
import { Arg, Mutation, Resolver } from 'type-graphql';
import { Author } from '../entities/author';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { AuthorInput } from '../types/author-input';

@Resolver((of) => Author)
export class AuthorResolver {
  constructor(
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(Author)
    private readonly authorRepository: Repository<Author>,
  ) {}

  @Mutation((returns) => Author)
  async createAuthor(@Arg('author') authorInput: AuthorInput): Promise<Author> {
    const author = this.authorRepository.create({ ...authorInput });
    return this.authorRepository.save(author);
  }
}
