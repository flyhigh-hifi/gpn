import { Connection } from 'typeorm';
import * as faker from 'faker';
import { createTestConnection } from '../../test/utils/createTestConnection';
import { gCall } from '../../test/utils/gCall';
import { Book } from '../entities/book';

let conn: Connection;

beforeAll(async () => {
  conn = await createTestConnection();
});
afterAll(async () => {
  await conn.close();
});

const createAuthorMutation = `
    mutation CreateAuthor($author: AuthorInput!) {
      createAuthor(author: $author) {
        id
        name
      }
    }
`;

const createBookMutation = `
    mutation CreateBook($book: BookInput!) {
      createBook(book: $book) {
        id
        name
        pageCount
        author {
          id
          name
        }
      }
    }
`;

const getBooksWithAuthorQuery = `
    query GetBooksWithAuthor {
      getBooks {
        id
        name
        pageCount
        author {
          name
        }
      }
    }
`;

const getBooksWithoutAuthorQuery = `
    query GetBooksWithoutAuthor {
      getBooks {
        id
        name
        pageCount
      }
    }
`;

describe('Book', () => {
  it('creates book', async () => {
    const author = {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    };

    const createAuthorResponse = await gCall({
      source: createAuthorMutation,
      variableValues: {
        author,
      },
    });

    const { id } = createAuthorResponse.data.createAuthor;

    const book = {
      name: faker.lorem.words(3),
      authorId: id,
      pageCount: faker.random.number(5000),
    };

    const createBookResponse = await gCall({
      source: createBookMutation,
      variableValues: {
        book,
      },
    });

    expect(createBookResponse?.data?.createBook).toMatchObject(book);

    const dbBook = await conn
      .getRepository(Book)
      .findOne({ where: { id: createBookResponse.data.createBook.id } });
    expect(dbBook).toBeDefined();
    expect(dbBook.name).toBe(book.name);
    expect(dbBook.author).toMatchObject(author);
  });

  it('gets books with w/o authors', async () => {
    const response = await gCall({
      source: getBooksWithoutAuthorQuery,
    });

    expect(response.data.getBooks).toHaveLength(1);
    expect(response.data.getBooks[0].author).toBeUndefined();
  });

  it('gets books with with authors', async () => {
    const response = await gCall({
      source: getBooksWithAuthorQuery,
    });

    expect(response.data.getBooks).toHaveLength(1);
    expect(response.data.getBooks[0].author).toBeInstanceOf(Object);
  });
});
