import { Field, InputType } from 'type-graphql';
import { Author } from '../entities/author';

@InputType()
export class AuthorInput implements Partial<Author> {
  @Field((type) => String)
  name!: string;
}
