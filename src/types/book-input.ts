import { Field, ID, InputType, Int } from 'type-graphql';
import { Book } from '../entities/book';

@InputType()
export class BookInput implements Partial<Book> {
  @Field((type) => String)
  name!: string;

  @Field((type) => Int)
  pageCount!: number;

  @Field((type) => ID)
  authorId!: number;
}
