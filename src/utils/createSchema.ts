import * as TypeGraphQL from 'type-graphql';
import { BookResolver } from '../resolvers/book';
import { AuthorResolver } from '../resolvers/author';
import { Container } from 'typedi';

export const createSchema = (opts = {}) => {
  return TypeGraphQL.buildSchema({
    resolvers: [BookResolver, AuthorResolver],
    container: Container,
    ...opts,
  });
};
