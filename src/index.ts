import 'reflect-metadata';
import { ApolloServer } from 'apollo-server';
import { Container } from 'typedi';
import * as TypeORM from 'typeorm';
import { ApolloServerLoaderPlugin } from 'type-graphql-dataloader';
import { getConnection } from 'typeorm';
import { createSchema } from './utils/createSchema';

TypeORM.useContainer(Container);

async function bootstrap() {
  try {
    await TypeORM.createConnection({
      type: 'mysql',
      database: process.env.MYSQL_DATABASE,
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      port: 3306,
      host: process.env.MYSQL_HOST,
      entities: [__dirname + '/entities/*.ts'],
      synchronize: true,
      logger: 'advanced-console',
      logging: 'all',
      dropSchema: true,
      cache: true,
    });

    const schema = await createSchema();
    const plugins = [
      ApolloServerLoaderPlugin({
        typeormGetConnection: getConnection,
      }),
    ];

    const server = new ApolloServer({ schema, plugins });

    const { url } = await server.listen(+process.env.PORT || 3000);
    console.log(`Server is running, GraphQL Playground available at ${url}`);
  } catch (err) {
    console.error(err);
  }
}

bootstrap();
