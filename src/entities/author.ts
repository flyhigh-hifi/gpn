import { Field, ID, ObjectType } from 'type-graphql';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { Book } from './book';

@ObjectType()
@Entity()
export class Author {
  @Field((type) => ID)
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @Field((type) => String)
  @Column({ type: 'varchar' })
  name!: string;

  @OneToMany((type) => Book, (book) => book.author)
  books!: Book[];
}
