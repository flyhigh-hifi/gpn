import { Field, ID, Int, ObjectType } from 'type-graphql';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne,
  JoinTable,
  RelationId,
} from 'typeorm';
import { Author } from './author';
import { TypeormLoader } from 'type-graphql-dataloader';
import { Lazy } from '../types/lazy';

@ObjectType()
@Entity()
export class Book {
  @Field((type) => ID)
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @Field((type) => String)
  @Column({ type: 'varchar' })
  name!: string;

  @Field((type) => Int)
  @Column({ type: 'int' })
  pageCount!: number;

  @Field((type) => Author)
  @ManyToOne((type) => Author, (author) => author.books, {
    cascade: true,
    lazy: true,
  })
  @TypeormLoader((type) => Author, (book: Book) => book.authorId)
  author!: Lazy<Author>;

  @Column({ type: 'int' })
  @RelationId((book: Book) => book.author)
  authorId!: number;
}
